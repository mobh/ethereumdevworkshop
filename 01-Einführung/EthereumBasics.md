

![Technische Einführung](../images/Slide_Teil1.png)

----
![HandsOn](../images/Theorie.png)

----
![Technische Einführung](../images/Slide_technischeEinfuehrung_grundkonzepte.png)

----
![Technische Einführung](../images/Slide_technischeEinfuehrung_grundkonzepte_detail.png)

----
![Technische Einführung](../images/Slide_technischeEinfuehrung_grundkonzepte_fragen.png)


----
## Grundkonzepte

### Hash
 Eindeutiges Prüfwert von fixe länge für beliebige digitale Daten ( Zeichenkette , Dateien..)
 >Im Blockchain Context werden hashes bei der Identifikation von Blocks, Transaktionen, Proof of work, Adressen, Smart contracts.. benutzt
```
// Message-Digest Algorithm 5 (MD5) , 128 
md5 -s "01.02.2018,von:Gustav,An:Alex,10,CHF"
//secure hash algorithm sha-256
openssl sha -sha256 <FileName.txt> 
// Beispiel openssl sha -sha256 "transaktionen.txt"

```
### Public & private keys
Public und Privat keys bieten die Möglichkeit:
- Daten zu verschlüsseln und zu entschlüsseln.
- Signaturen von Daten zu erstellen , und dieser Signaturen zu prüfen ( Verify).

#### Erstellen Public & private key Pair 
```
// Create RSA private key
openssl genrsa -des3 -out my-rsa-key.pem 2048
openssl rsa -in my-rsa-key.pem -outform PEM -pubout -out my-rsa-key-pub.pem

```

#### Encrypt / Decrypt
Szenario: Sender will ein Meldung an Empfänger senden, die nur vom Empfänger gelesen werden kann

* Empfänger veröffentlicht sein Public Key
* Sender verschlüsselt die Daten mit dem Empfänger Public Key
* Nur der Empfänger kann mi seinem Private Key den Inhalt entschlüsseln

```
// encrypt file transaktionen.txt
openssl pkeyutl -encrypt -pubin -inkey my-rsa-key-pub.pem -in transaktionen.txt -out transaktionen.encrypted
// decrypt file transaktionen.txt
openssl pkeyutl -decrypt -inkey my-rsa-key.pem -in transaktionen.encrypted -out  transaktionen_decrypted.txt
```

#### Sign / Verfiy
Szenario: Empfänger will sicher sein, dass die Meldung tatsächlich vom Sender ist

* Sender veröffentlich sein Public Key
* Sender "hasht" die Meldung, dann signiert den Hash mit seinem Private key ( verschlüsselt den Hash der Meldung)
* Sender sendet die Meldung ( in Klar Text) + seine Signatur
* Empfänger verifiziert die Signatur mit Hilfe des Sender's Public key

----
![ Public & private key](../images/Silde_Elemente_von_Blockchain_PKI.png)
----

```
//1 HASH
shasum -a 256 transaktionen.txt > transaktionen_klartext_hash

result:84beaf9892e379a596cf469e6ab5f14e7169238255c6379b04f51c7d7d8ba094
//2 Sign
openssl rsautl -inkey my-rsa-key.pem -sign -in transaktionen_klartext_hash -out transaktionen_signatur

//3  Verify (vergleich vom Hash)
openssl rsautl -inkey my-rsa-key-pub.pem -pubin -in transaktionen_signatur

result:84beaf9892e379a596cf469e6ab5f14e7169238255c6379b04f51c7d7d8ba094
```


>Im Blockchain context, sind alle Transaktionen (vom message Sender) "signed" , und können von den alle Nodes verifiziert werden 


### Blockchain Account
Ein Account ist eine (20 bytes) Adresse, dieser wird aus dem Public Key abgeleitet

Beispiel Ethereum: 

```
// Aus dem Ethereum Yellow Papers, die Adresse , wird in 3 Schritte erzeugt:
1.Create a random private key ( 256 bits)
2.Derive the public key from this private key
3.Derive the address from this public key. (160 bits / 20 bytes)

```


----

![Blockchain_Elements](../images/Silde_Elemente_von_Blockchain.png)
----

![Blockchain_Elements](../images/Silde_Elemente_von_Blockchain_detail.png)
----


## Elemente von Blockchain

### Blockchain 
Aus technische Sicht ist Blockchain eine Kombination von verschiedenen Technologien:

- Es ist eine Liste von Datensätzen (Blöcke), die mit Hilfe von Hashes verkettet sind
- Die Daten innerhalb der Blöcke sind Transaktionen die mit einem Zeitstempel versehen und signiert sind. Ein nachträgliches Manipulieren bestehende Daten ist nicht möglich  
- Jeder im Netzwerk hat die gleiche Kopie der Datenbank (Distributed P2P)  
- Neue Blöcke / Daten werden über ein automatisches Konsens-verfahren zwischen den Nodes angehängt ( Beispiel Proof of work) , keine zentrale Master Server ist dafür nötig

### Blocks
Container von Transaktionen + BlockHeader für die Identifikation

![Blockchain_Elements](../images/Silde_Elemente_von_Blockchain_Blocks.png)

### P2P Network/ Nodes
Im Gegensatz zum traditionellen Client Server Netzwerk, gibt es im P2P kein zentralen Server.
Alle Teilnehmer in einem P2P Netzwerk sind gleichberechtigt, jede hat eine Kopie der Daten, und kann neue Daten aus verschiedenen Nodes im Netz beziehen bzw. an andere Nodes senden ( Blockchain)

### Mining
Hinzufügen von Blöcken mit validierten Transaktionen an einer bestehenden Blockhain.

Dies erfolgt in der Regel über ein Proof-of-Work (bei Ethereum über Proof of work +  Proof of Stake in Zukunft) 
Beim Proof of Work versuchen die Miner zu einem Hash Output mit gewissen Eigenschaften den zugehörigen Input zu finden. 
Der erste Miner, der die Lösung gefunden hat, erhält ein Block Reward.

Beim Minen werden je nach Blockchain,  neben dem Block Reward auch neue Coins (5 Ether / Block) erzeugt, und an den Miner ausbezahlt.

### Cryptocurrency
Fundamentales Bestandteil von Public Blockchains:
- Transaktionsgebühren für Teilnehmer 
- incentive für den Miner werden damit bezahlt
- Schutz von "Denial of Services attacks"
- Schutz von Fehlern in Programmen wie z.B endless loops

```
Beispiel Ethereum: https://etherscan.io/stat/supply
```
### Transaction
Schreib Operation auf dem Blockhain Netzwerk, es betrifft 2 Accounts ( from , to ) und kostet Gebühren

```
Beispiel: https://etherscan.io/tx/0xa70ab41ca18f008b6d6f715e673886cd905e305228cea58ce812ffd5dec2383c
```


### Smart Contract
Ein Programm was auf der Blockchain "deployed" ist, und was in der Lage ist, digitale Assets zu verwalten und Transaktionen durch zu führen 

Beispiel:
```
https://www.stateofthedapps.com/dapps/flight-delays-suck
//Web:
https://fdd.etherisc.com
https://github.com/etherisc/flightDelay/blob/master/contracts/FlightDelayLedger.sol
```

#### Token
Digitale Assets die über Smart contracts implementiert werden können.


----

![Technische Einführung](../images/Slide_technischeEinfuehrung_EthereumStack.png)

----
## Ethereum Stack

![Technische Einführung](../images/Slide_technischeEinfuehrung_EthereumStack_detail.png)


----
![ Public & private key](../images/Slide_technischeEinfuehrung_InstallationEthereum.png)
----

----
![HandsOn](../images/HandsOn.png)

## Installation und Konfiguration des Client (Geth)

### Geth installieren
#### auf Mac:

https://github.com/ethereum/go-ethereum/wiki/Installation-Instructions-for-Mac

* start terminal

* Install homebrew
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

* Install geth
```
brew tap ethereum/ethereum
brew install ethereum
```
( Wenn bereits installiert Upgrade durchführen "brew upgrade ethereum")

#### auf Windows:

https://github.com/ethereum/go-ethereum/wiki/Installation-instructions-for-Windows

* zip file runterladen 
* extract geth.exe 
* starten command prompt
```
cmd
```

* Verzeichnis wechseln 

```
cd <Download Verzeichnis>
```


* Starten Sie die Software Installation:

```
geth.exe
```

* Geben Sie C:\Geth als Installationsverzeichnis ein:

```
C:\Geth
```

* Pfad der Installation (C:\Geth) in der UmgebungsVariable ergänzen 
```
setx path "%path%;C:\Geth"
```

![LetsTry](../images/LetsTry.png)

----
![HandsOn](../images/HandsOn.png)
----

####  Prüfen der Installation und die Version von Geth

```
geth version
```

####  connect to "Main net" (downloads live chain)

* starten des Nodes auf dem mainNet
```
geth console
```

* Mit dem Befehl admin.peers in der console kann man prüfen zu welchen nodes im P2P Netzwerk , der eigene Node gerade connected ist

```
admin.peers
```

* Check alle Nodes im Internet "Main Net"
```
https://www.ethernodes.org/network/1
```

* Cancel nach 1 - 2 Minuten ( Ctrl + C )
(Die gesamte BlockChain data ist mehrere GB gross, und wächst täglich wächst weiter)
>https://etherscan.io/chart2/chaindatasizefast 

* prüfen der Blockchain data vom "Main net" auf dem lokalen Rechner unter:
```
//auf Mac:
/Users/XXXmyUserNameXXX/Library/Ethereum/geth/ 

//auf Windows:
C:\Users\XXXmyUserNameXXX\AppData\Roaming\Ethereum\geth\chaindata
(AppData ist ein "Hidden" Directory, es kann über das Menü " View -> Hidden Files" und die CheckBox im Filexplorer aktiviert werden) 

```


#### (Optional) Connect to "Test net" (downlaods test chain)

```
geth --testnet 
```
* Cancel nach 1 - 2 Minuten ( Ctrl + C ) 
* prüfen Blockchain data vom "Test Net" unter:
```
//auf Mac:
/Users/XXXmyUserNameXXX/Library/Ethereum/geth/ 

//auf Windows:
C:\Users\XXXmyUserNameXXX\AppData\Roaming\Ethereum\testnet\geth\chaindata
```

* Check Nodes im Internet "Test Net"
```
https://www.ethernodes.org/network/2
```


![LetsTry](../images/LetsTry.png)


----

![Technische Einführung](../images/Slide_technischeEinfuehrung_BlockchainBauen.png)

----
![HandsOn](../images/HandsOn.png)
----

## lokales Blockchain bauen

### Umgebung vorbereiten

#### Neues Verzeichnis erstellen:


```
//auf Mac:
cd ~
mkdir ethereum

//auf Windows:
md c:\ethereum
cd c:\ethereum
```

#### Path in ENV variables ergänzen:
```
//auf Mac:
echo 'export ethereum_home=/Users/XXXmyUserNameXXX/ethereum' >> ~/.bash_profile
export ethereum_home=/Users/XXXmyUserNameXXX/ethereum

//auf Windows:
setx ethereum_home "C:\ethereum"
set ethereum_home=C:\ethereum 
echo %ethereum_home%

```

#### Mit einem Text Editor den genesis File erzeugen 

```
//auf Mac:
z.B TextPad oder SublimeText

//auf Windows:
z.B NotePad++ oder SublimeText oder ultraEdit

```

####  folgenden json Struktur in dem File "genesis26.json" speichern
```json
{
  "config": {
        "chainId": 26,
        "homesteadBlock": 0,
        "eip155Block": 0,
        "eip158Block": 0
    },
    "alloc": {},
  "difficulty" : "0x4000",
  "extraData"  : "",
  "gasLimit"   : "0x2fefd8",
  "nonce"      : "0x0000000000000026",
  "mixhash"    : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "parentHash" : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "timestamp"  : "0x00"
}
```


####  init "first block" auf dem node1
``` 
//auf Mac:
geth --datadir $ethereum_home/node1 init $ethereum_home/genesis26.json

//auf Windows:
geth --datadir %ethereum_home%/node1 init %ethereum_home%/genesis26.json
```

> Ethereum Genesis Block hat 8893 Tx:  https://etherscan.io/txs?block=0 

####  starten der Console und Interaktion mit node 1
```
//auf Mac:
geth --datadir $ethereum_home/node1 --networkid 26 console

//auf Windows:
geth --datadir %ethereum_home%/node1 --networkid 26 console

```
> ein andere port kann über den param. -port gesetzt werden z.B "35555", die Default Listener Port bei Geth ist 30303

* Liste der verfügbaren commands

```
eth
```


![LetsTry](../images/LetsTry.png)



----

![Technische Einführung](../images/Slide_technischeEinfuehrung_Interaction.png)
----

![HandsOn](../images/HandsOn.png)
----

## Interaktion mit dem Blockchain via CLI

### Commands ausführen über Geth JavaScript console

#### Accounts erstellen 

```
personal.newAccount()  
```
- Pass Phrase merken 
- Für echte Accounts, den KeyStore File Backupen ( unter Node/keyStore zu finden)
```
Format  UTC--{Jahr}-{Monat}--{account}
```
Dieser enthält den Privat Key verschlüsselt mit dem Pass Phrase.

Der Private Key kann später aus dem KeyStore und
den Pass Phrase wieder hergestellt werden, ohne PassPhrase wäre der Key unverschlüsselt in dem KeyStore
```
{"address":"14631856ad24c58f340fd79ebada5a9348abc035","crypto":{"cipher":"aes-128-ctr","ciphertext":"072d1a7873fdeddf223834bed3adb028709a46aa3aa84ba05d2106c23edfb31b","cipherparams":{"iv":"af73d8460a5c9db02fbe00e2c847c425"},"kdf":"scrypt","kdfparams":{"dklen":32,"n":262144,"p":1,"r":8,"salt":"e7ddebfcb75be169c7a58043855a74ade4bec7d5c795f8e7c53bafe966ce73ba"},"mac":"2ab7d4b81c41c9be5ec56d8bc993e19c5ccc3671aae656d18ba3e71415c19160"},"id":"0c148ffd-e8aa-4c7a-bd50-84bd6b3dacbd","version":3}

```
Der Public Key kann aus der Account ermittelt werden ( wenn eine Transaktion durchgeführt wird)

>ein Ether Walltet macht das gleich über ein UI, Personal.* ist nur lokal möglich

#### Accounts prüfen 

```
eth.accounts
eth.getBalance(eth.accounts[0])
// Tip für Windows auf Qwertz (swiss german)
[] Taste = ctrl+alt+ü+!
```

#### Mining 


Mining starten

Mining benutzt eth.coinbase für die mining rewards. eth.coinbase, wenn nicht spezifiziert ist der default eth.accounts[0]

```
eth.coinbase
personal.unlockAccount(eth.accounts[0])
miner.start(1)
```
> Hinweis: DAG Generation kann beim ersten mining länger dauern (je nach eingestellte "difficulty", 5-10 minuten)

>Der DAG wird benötigt um den Proof of Work zu berechnen.
  Details sind unter https://github.com/ethereum/wiki/wiki/Ethash-DAG  
& https://github.com/ethereum/wiki/blob/master/Dagger-Hashimoto.md  zu finden )
```
miner.stop()
eth.blockNumber 
```

#### Transfers 

* Ein zweiten account account2 erstellen  (mit personal.newAccount() analog ersten Account )
* Ether transferieren aus account1 nach account2 ( mining nicht vergessen)

```
// Transfer 100 Wei
eth.sendTransaction({ from:eth.accounts[0], to:eth.accounts[1], value: 100})
// (Optional) Beispiel mit Transaction Hash
eth.getTransaction(<Transaction>)
eth.getTransactionReceipt(<Transaction Hash>)
```
Einheiten in ether 

```
Einheit	              Wert in Wei
wei                   1 wei	  1
Kwei (babbage)        1e3 wei	  1,000
Mwei (lovelace)       1e6 wei	  1,000,000
Gwei (shannon)        1e9 wei	  1,000,000,000
microether (szabo)    1e12 wei    1,000,000,000,000
milliether (finney)   1e15 wei    1,000,000,000,000,000
ether                 1e18 wei    1,000,000,000,000,000,000
```

#### attach neue console (optional) 
* attach neue console  ( in einem neuen Terminal Fenster) 

```
//auf Mac:
geth attach ipc:$ethereum_home/node1/geth.ipc

//auf Windows:
geth attach \\.\pipe\geth.ipc
oder  geth attach http://localhost:8545 ( falls IPC auf Windows nicht funktionniert)

```

#### weitere eth Befehle 

```
net.version
eth.accounts
eth.mining
eth.blockNumber
eth.getBlock(0)
// Prüfen wieviel gaz von der transaktion konsumiert würde
eth.getTransactionReceipt("<txHash>")

```
* liste alle verfügbaren befehle
>TIP:  Alle verfügbaren Befehle können mit der Eingabe von 2X die "Leerschlag Taste" (spaces) und dann 2 X die "Tab Taste" angezeigt werden. 

* exit console
```
exit
```


![HandsOn](../images/LetsTry.png)

----
![HandsOn](../images/HandsOn.png)
----



## Zugriff über web3 (Optional)
### Commands ausführen mit JSON über Web3 library ( Dapps, Web Applikationen)

Web3 ist ein JavaScript Library. Dieser erlaubt eine einfacheren Zugriff auf die Ethereum Nodes. Web3 ist automatisch instanziert in der Geth Console.  Wenn die Web3 Library in externen Applikation benutzt wird, muss dieser geladen und konfiguriert werden.


#### "Utils" Funktionen

* Konvertiere Decimal nach Hex
```
web3.toHex(123)
```

* Konvertiere von wei to ether // from Ether to Wei

```
web3.toWei(2,"ether")
web3.fromWei(eth.getBalance(eth.accounts[0]), "ether")
// Beispiel sendTransaction
// Transfer 2 Ether
eth.sendTransaction({ from:eth.accounts[0], to:eth.accounts[1], value: web3.toWei(2,"ether")})
// Transfer Ether mit einem Message 
eth.sendTransaction({from:eth.accounts[0],to:eth.accounts[1],value:web3.toWei(1,'finney'),data:web3.toHex('Ich bin mal Weg!')})

```

#### Block & Mining Funktionen
* Anzahl von hashes pro Sekunde beim mining von einem Node 
```
web3.eth.hashrate
```

* Median des gaz Price der letzten Blöcke 

```
web3.eth.gasPrice
```

#### Alle Web3 methoden anzeigen

>In dem gleichen console ausführen, wo geth aktiv ist
```
web3
```


#### Dokumentation
https://web3js.readthedocs.io/en/1.0/index.html

#### Exit 

```
exit
```

![HandsOn](../images/LetsTry.png)

----
![HandsOn](../images/Uebung.png)
----
## Übung 1: 
Erzeuge ein zweites Geth Node, führe Transaktionen durch und prüfe ob, die Blocks nach dem Mining auf beide Nodes vorhanden sind

----
![HandsOn](../images/Uebeung_p2p.png)
----

### Schritte /Hints

#### Node2 erstellen und starten
* Neue Terminal Console (cmd) starten und Node 2 erzeugen (Console2) 
```
//auf Mac:
geth --datadir $ethereum_home/node2 init $ethereum_home/genesis26.json

//auf Windows:
geth --datadir %ethereum_home%/node2 init %ethereum_home%/genesis26.json
```

* Starten von Node2 auf ein anderen Port mit der gleichen Netzwerk-Id

```
//auf Mac:
geth --datadir $ethereum_home/node2 --port 30304 --networkid 26 console

//auf Windows:
geth --datadir %ethereum_home%/node2 --port 30304 --networkid 26 console --ipcdisable 
```
>um den Logging Level von Geth beim ausführen von Befehlen zu erhöhen, kann der "verbosity" parameter benutzt werden z.B "-verbosity 3"


#### Peer to Peer (P2P) Netz erstellen
* Stoppen Node 1 und erneut starten (console1)
```
//auf Mac:
exit
geth --datadir $ethereum_home/node1 --networkid 26 console

//auf Windows:
exit
geth --datadir %ethereum_home%/node1 --networkid 26 console

```

* Peer to Peer Netz erstellen, mit Hilfe der Funktion nodeInfo und addPeer


```
//Console von Node 2: Ermitteln "enode" von Node2 über die Funktion nodeInfo ( "enode" in der Zwischen-Ablage kopieren)
admin.nodeInfo 

//Console von Node1: kopierte "enode"  als parameter der Funktion admin.addPeer auf Node1 ausführen
admin.addPeer(<"enode">)

// Prüfen ob addPeer funktioniert hat
admin.peers

```
  Beispiel: admin.addPeer(enode: "enode://a02e191689b1b5281eac6739b7abc05332a4b92d0e89289d987b02a88719fc2615bf85228c8a5e33a38d22828eb1bdd80d283c37ba29c0187c5ea6bcf4582e09@[::]:30304?discport=0")

#### Mining auf dem Peer to Peer (P2P) Netz starten

*  Prüfen , blockNumber auf beide Nodes
```
eth.blockNumber
```
-Start Mining on Node 1 
```
miner.start(1)
```
-Prüfen block Number und chainData auf beide Nodes
```
eth.blockNumber  ( auf beide nodes)
```

-(optional) Transaction (transfer) durchführen und prüfen , welche node den Block schreibt

----
![HandsOn](../images/Theorie.png)
----

## Zugriff über RPC (Optional)
### Commands ausführen mit JSON über RPC ( für remote access)

#### HTTP Post Request auf dem Blockchain ausführen

* Restart node 1 ( 8545 ist der default wert für RPC)
```
//auf Mac:
geth --datadir $ethereum_home/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console

//auf Windows:
geth --datadir %ethereum_home%/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console

```


* Neues terminal (console) eröffnen und den command "eth_accounts" als HTTP POST Request über Curl ( Client for URLs) ausführen
```
curl -X POST -H "Content-Type: application/json" --data '{"jsonrpc":"2.0","method":"eth_accounts","params":[],"id":2}' localhost:8545 -v
```
> Beispiel vom Return Wert:
{"jsonrpc":"2.0","id":2,"result":["0x685182506beb15c93d4f6ce4a779912ab1857206","0x8527c3b775881168c3825c029418e3a501119bee"]}


