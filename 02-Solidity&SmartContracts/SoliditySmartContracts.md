

----

![Solidity & SmartContracts](../images/Slide_Teil2.png)

----
![Solidity & SmartContracts](../images/Slide_Solidity_SmartContracts_Remix.png)

----
![HandsOn](../images/Theorie.png)
----
## Remix IDE
----
![Solidity & SmartContracts](../images/Slide_Solidity_SmartContracts_Remix_Detail.png)

----


### Remix Editor starten

```
Online Version: 
http://ethereum.github.io/browser-solidity
oder
https://ethereum.github.io/browser-solidity  ( über https)

//auf Mac
Bitte Safari oder Chrome Browser benutzen

//auf Windows
Bitte Chrome Browser benutzen ( IE funktioniert nicht)
```
>Hinweis: Falls Online nicht möglich, IDE lokal installieren:
https://github.com/ethereum/browser-solidity
( git clone  / npm install / npm start / http://127.0.0.1:8080)

----

![Solidity Language](../images/Slide_Solidity_SmartContracts_Solidity_Greeter.png)

----

![Solidity Language](../images/Slide_Solidity_SmartContracts_Solidity_Detail.png)

----


----
![HandsOn](../images/HandsOn.png)
----

## Solidity Sprache mit Smart Contract Beispiel "Greeter"

```
pragma solidity ^0.4.0;

contract Greeter {
    //***********************************************************************
    // Demo code wird während des Kurses vom Trainer ergänzt und erklärt
    //***********************************************************************
}


```
----
![Solidity & SmartContracts](../images/Slide_Solidity_SmartContracts_DeployLocalChain.png)

----
## Contract "Greeter" auf Blockchain deployen

### Restart node1  
```
//auf Mac:
geth --datadir $ethereum_home/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console

//auf Windows:
geth --datadir %ethereum_home%/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console

```

>oder anstatt "*" den domain als parameter definieren ( damit hat nur der domain "remix.ethereum.org"  Zugriff auf port 8545)
    
>geth --datadir $ethereum_home/node1 --rpc --rpcport 8545 --rpcaddr 127.0.0.1 --rpccorsdomain "http://remix.ethereum.org" --rpcapi "eth,net,web3" console


###  Unlock Account 
```
personal.unlockAccount(eth.accounts[0])
```


### Deploy Greeting contract
- selektiere web3.provider and gebe http://localhost:8545 ein 
>falls localhost auf windows nicht funktioniert ( hosts datei) , benutzen Sie  http://127.0.0.1:8545  

( version 0.4.18 benutzen) 

    > Warnings & errors korrigieren , falls eine neuere Version benutzt wird.

- Click "create" Contract um den contract zu deployen (mit dem Parameter "Hello World" )

    > Bitte beachten Sie dass, localhost und Remix den gleichen protocol benutzen (http oder https) 


----
![Remix & deploy](../images/remix_create.png)
----

#### Mining der Transaktion 

- Prüfe die pending Transaction

```
eth.pendingTransactions
```


-  start mining um den Contract zu deployen
```
miner.start(1)
```

Der konsumierte gaz preis wird vom Account des contract-Aufrufers abgezogen ( "gaz Limit" ist ein Schutz gegen Programm-Fehler) 

- führen Sie die Funktionen des contractes über den remix UI auf
- Prüfen Sie welcher Funktionen , ein State Änderung verursachen , d.h eine Transaction auf dem chain erfordern inklusive mining (gaz) und welcher nicht.

>Um den Transaktionskosten ( gaz) zu prüfen ist es auch möglich die Balance vor und nach dem deploy zu prüfen.
 >Deploy über Account1 und prüfen Balance im Account1 , da die mining Rewards auf Account0 laufen
eth.getBalance(eth.accounts[1])


-  stop mining
```
miner.stop()
```

----
## Optionale Tests 
* attach neue console  ( in einem neuen Terminal Fenster) 

```
//auf Mac:
geth attach ipc:$ethereum_home/node1/geth.ipc

//auf Windows:
geth attach \\.\pipe\geth.ipc
Oder  geth attach http://localhost:8545 ( falls IPC auf Windows nicht funktionniert)

```
* check byteCode of the deployed contract

```
eth.getCode ("<Adress from remix>")

```

*  debug ist möglich in remix ( set Env = JavaScript VM)


![LetsTry](../images/LetsTry.png)

----
![Solidity & SmartContracts](../images/Slide_Solidity_SmartContracts_ShopContract.png)
----
![HandsOn](../images/HandsOn.png)
----

## SmartContract Beispiel Shop:

Unsere Demo Shop soll folgende 2 Funktionen unterstützen:

    - addProduct ( für den Shop Owner um Produkte hinzufügen)
    - buyProduct ( für Kunden um Produkte zu kaufen, und mit der Standard Ethereum Währung zu bezahlen)

```
pragma solidity ^0.4.18;

contract Shop { 

    //***********************************************************************
    // Demo code wird während des Kurses vom Trainer ergänzt und erklärt
    //***********************************************************************

    // add variable to store the owner
	
    // define modifier for Owner
	modifier isOwner(){
	    
	}

    // define product struct
	 

    // Constructor
    function Shop() public {
    	// Set owner
   
    }

    // add a Map to store all products

   

    // Only owner can add product
    function addProduct(string _name, uint _uid, uint _price, uint _stock ) public returns (bool success) {
    	
        // Check if Owner
       
    }

    // Anyone can buy product  ( reading and writing to the storage costs )

   function buyProduct(uint productId)  public payable {
     
      
        	//adapt stock
      
            // give the Money back to sender in case of error or rest amount
           
     }	


}


```
set gaz limit = 3000000 ( in remix)


![LetsTry](../images/LetsTry.png)

----
![HandsOn](../images/Uebung.png)
----
## Übung 2 
Baue dein ersten smart contract mit Hilfe der folgenden Template ( Shop contract vom vorherigen Demo ausbauen).

-  Kopiere die aufbereitete contract template aus dem "99-Solutions" Verzeichnis im BitBucket Repo in dem remix editor
-  Die 2 Funktionen sollen analog zur DEMO fertig implementiert werden:

    - addProduct 
    - buyProduct 

- Füge eine neue Funktion in dem Shop:
   - withdraw : Mit der Funktion soll der Shop Owner die Einnahmen an sich überweisen ( nur vom Shop Owner aufrufbar)
 
    

Template:

```
contract Shop {

	struct Product {
        string name;
        uint uid;
        uint price;
        uint stock;
    } 

        // define modifier for Owner
	modifier isOwner(){
	    
	}

    // define product struct
	 

    // Constructor
    function Shop() public {
    	// Set owner
   
    }

    // add a Map to store all products

   

    // Only owner can add product
    function addProduct(string _name, uint _uid, uint _price, uint _stock ) public returns (bool success) {
    	
        // Check if Owner
       
    }

    // Anyone can buy product  ( reading and writing to the storage costs )

   function buyProduct(uint productId)  public payable {
     
      
        	//adapt stock
      
            // give the Money back to sender in case of error or rest amount
           
     }	



    // Owner can withdraw the Money from the contract to his Address
     function withdraw() isOwner public{
         // Use Transfer method
         
     }		
}
```

### Schritte /Hints
- addProduct: Zunächst wird ein Produkt generiert, und danach in dem mapping "products" hinzugefügt werden.
- buyProduct: Der Betrag für den Kauf des Produkts wird nicht als Parameter der Funktion , sondern wird mit der Transaktionsmessage
(msg.value) mit geliefert, falls Produkt verfügbar ( stock >0) , soll nach dem Verkauf die Stock menge um 1 verringert werden.   
- withdraw : benutze die Transfer Method ( und nicht send)

> Solidity Dokumentation:  http://solidity.readthedocs.io/en/develop/miscellaneous.html

----
### SHOP contract auf dem lokalen Blockchain deployen

- Selektiere web3.provider in dem remix browser, und geben Sie http://localhost:8545 ein

### unlock account ( console)
```
personal.unlockAccount(eth.accounts[0])
```
### Contract Funktionen aufrufen und testen (  produkt im shop hinzufügen, produkt kaufen)


----
![Solidity & SmartContracts](../images/Slide_Solidity_SmartContracts_DeployMainNet.png)

----
![HandsOn](../images/Theorie.png)
----

## Deployment auf testNet und MainNet (Optional)

Um auf dem testNet ( Rinkeby) oder auf dem mainNet ( produktiv) deployen zu können, benötigt man ein Account (newAccount).

- Für den TestNet kann man über ein Faucet test ether generieren , z.B 
```
https://www.rinkeby.io/#faucet
```
- Für den mainNet benötigt man echte Ether, die kann man nur "minen" oder auf einem Exchange mit Bitcoins oder echtes Geld kaufen
Das Deployment kann über Command Line erfolgen:
```
geth --rinkeby console 2>> ./rinkeby.log
```

```
personal.unlockAccount(eth.coinbase)
```

```
var getABIDefiniton = eth.contract(<ABI definiton>)
```
```
var bytecode = '0x<Byteode>'
```
```
var deploy = {from:eth.coinbase, data:bytecode, gas: 2000000}
```
```
var myContract = getABIDefiniton.new("myContract", deploy)
```
Die JSON ABI Definition und der compiled byte code können aus remix UI übernommen werden, oder mit dem "solc" compiler erzeugt werden 

----
## Ausblick  (Workshop2 Tokens + DApp ): 

- Baue ein eigenes Token: loyalityCoin contract nach dem ERC20 Standard
- Ergänze den ShopContract, so dass Kunden in der lage sind bei jedem Kauf loyalityCoins zu sammeln, und mit einer neuen Funktion  "buyProductWithLoyaltyCoin" , ihre Coins für den Kauf für Produkte zu nutzen 
- UnitTests bauen für den SmartContract
- Entwickle eine einfachen Web Applikationen  (HTML + JS) für dein  WebShop Contract und integriere die Funkion "buyProduct" in deine WebAPP
 