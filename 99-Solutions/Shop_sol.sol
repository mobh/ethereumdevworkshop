pragma solidity ^0.4.18;

contract Shop { 

	address public owner;
	
	modifier isOwner(){
	    require (msg.sender == owner);
	    _;
	}

	struct Product {
        string name;
        uint uid;
        uint price;
        uint stock;
       
    } 

    // Constructor
    function Shop() public {
    	owner  = msg.sender;
    }

    mapping(uint => Product) public products;

    // Only owner can add product
    function addProduct(string _name, uint _uid, uint _price, uint _stock ) public returns (bool success) {
    	
        if (msg.sender == owner){
        	Product memory newProduct  = Product({ name: _name, uid: _uid , price: _price, stock: _stock });
	    	products[_uid] =  newProduct;
	    	return true;
	    }else{
        	revert();

        }
        return false;
    }

    // Anyone can buy product  ( reading and writing to the storage costs )

   function buyProduct(uint productId)  public payable {
     
       require ( msg.value >= products[productId].price);
       Product storage p = products[productId]; 
        uint rest;    

        if (p.uid >0  && msg.value >= p.price && p.stock > 0 ){
        	//adapt stock
        	p.stock --;
        	rest = msg.value - p.price;
        	 // give the Money back to sender
           	if (rest > 0){
			    msg.sender.transfer(rest);
			}
        	
        }else{
           

        }
     }	

    // Owner can withdraw the Money in the contract 
     function withdraw() isOwner public{
         // Use Transfer ( new method)
         owner.transfer(this.balance); 
     }	

}