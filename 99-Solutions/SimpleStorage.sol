pragma solidity ^0.4.18;

contract SimpleStorage{
 	// Declare state variables outside function, persist through life of contract
    uint storedData;
    
    function getStoredData() public constant returns (uint){
        
        return storedData;
    }
    
    function setStoredData (uint x) public {
        storedData = x;
        
    }
    
    
}