# Ethereum Developer Workshop

Dokumentation für den Ethereum Einsteiger Workshop für Software Entwickler

![Ethereum Basel](images/Ethereum_Basel.png)

## Entwickle mit der Ethereum Blockchain

----
### Ziel:​
Teilnehmer arbeiten mit lokaler Blockchain und deployen selbst entwickelten Smart-Contract.
​
### Form:
Gemeinsames Durcharbeiten der Schritte mit kurzen Erläuterungen und laufender Unterstützung.
​
### Zielgruppe:
Software Entwickler, die einen schnellen Einstieg in die Entwicklung mit der Blockchain Plattform (Ethereum) wünschen. 
​
### Voraussetzung: 
Basiswissen Programmierung. Eigener Laptop mit Installationsrechten. (Unsicher? Frag einfach einen Trainer: Jonas Felix)
​
### Abgrenzung:
Das Training fokussiert sich auf den Ethereum Stack für Entwickler.
